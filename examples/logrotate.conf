# To the extent possible under law, Eric Wong has waived all copyright and
# related or neighboring rights to this examples
#
# example logrotate config file, I usually keep this in
# /etc/logrotate.d/yahns_app on my Debian systems
#
# See the logrotate(8) manpage for more information:
#    https://linux.die.net/man/8/logrotate

# Modify the following glob to match the logfiles your app writes to:
/var/log/yahns_app/*.log {
	# this first block is mostly just personal preference, though
	# I wish logrotate offered an "hourly" option...
	daily
	missingok
	rotate 180
	compress # must use with delaycompress below
	dateext

	# this is important if using "compress" since we need to call
	# the "lastaction" script below before compressing:
	delaycompress

	# note the lack of the evil "copytruncate" option in this
	# config.  yahns supports the USR1 signal and we send it
	# as our "lastaction" action:
	lastaction
		# systemd users do not have PID files,
		# only signal the @1 process since the @2 is short-lived
		# and only runs while @1 is restarting.
		systemctl kill -s SIGUSR1 yahns@1.service

		# assuming your pid file is in /var/run/yahns_app/pid
		pid=/var/run/yahns_app/pid
		test -s $pid && kill -USR1 "$(cat $pid)"
	endscript
}
