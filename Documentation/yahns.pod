=head1 NAME

yahns - multi-threaded, non-blocking application server for Ruby

=head1 SYNOPSYS

yahns -c CONFIG_FILE [-D|--daemonize]

=head1 DESCRIPTION

L<yahns(1)> is the primary interface for launching a yahns application
server.  The configuration file is documented in L<yahns_config(5)>.
yahns hosts Rack HTTP applications, but may support others in the
future such as DRb.

=head1 SIGNALS

The following UNIX signals may be sent to the running yahns process.
If yahns is configured for worker_processes (optional), signals should
only be sent to the master process.

=over

=item INT/TERM/QUIT

Graceful shutdown.  If repeated (any of these signals
is sent twice), shutdown occurs immediately.

=item USR1

Reopen all logs owned by the master and all workers.
This scans the Ruby ObjectSpace for all open File objects with the
O_APPEND file flag and buffering disabled (IO#sync==false)

=item USR2

Reexecute the running binary.  A separate QUIT should be sent to
the original process once the child is verified to
be up and running.

=item HUP

If worker_processes are not used, this will reexecute the running
binary and gracefully exit the running process.  If worker_processes are
used, this will reload config file, app, and gracefully restart all workers

=item WINCH

Gracefully stops workers but keep the master running.
This will only work for daemonized processes and only if the
worker_processes configuration directive is used.

=item TTIN

Increment the number of worker processes by one
(only if the worker_processes directive is used)

=item TTOU

Decrement the number of worker processes by one
(only if the worker_processes directive is used)

=back

=head1 ENVIRONMENT

L<yahns(1)> itself requires no special environment variables.  However,
environment variables such as RACK_ENV and RAILS_ENV can affect Rack and
Rails applications it hosts.  Ruby and C library implementation-specific
environment variables will also affect it.

yahns will update the PWD (current working directory) env if the
working_directory directive is set (see L<yahns_config(5)>).

LISTEN_FDS and LISTEN_PID variables are used in our emulation
of L<sd_listen_fds(3)> function.   See L<sd_listen_fds(3)> manpage
for more details.

=head1 FILES

See L<yahns_config(5)> for documentation on the configuration file format.

=head1 CONTACT

All feedback welcome via plain-text mail to L<mailto:yahns-public@yhbt.net>
No subscription is necessary to email us.
Mail archives are available at L<https://yhbt.net/yahns-public/>

=head1 COPYRIGHT

Copyright (C) 2013-2016 all contributors L<mailto:yahns-public@yhbt.net>
License: GPL-3.0+ L<http://www.gnu.org/licenses/gpl-3.0.txt>

=head1 SEE ALSO

L<yahns-rackup(1)>, L<yahns_config(5)>, L<sd_listen_fds(3)>
